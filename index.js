// console.log("Notes Take Consol");
showNotes();

// validation of txtbox(if blank then not save) 
function keyUpFunc() {
    addTxt = document.getElementById("addTxt").value;
    // console.log(addTxt.length)
    if (addTxt.length > 0) {
        // console.log("Enable");
        document.getElementById("addBtn").disabled = false;
    }else{
        document.getElementById("addBtn").disabled = true;
    }
}


// on  btn click save 
let addBtn = document.getElementById("addBtn");
addBtn.addEventListener("click", function (e) {
    // get the value from addText id  on click event
    // console.log("click ");
    let addTxt = document.getElementById("addTxt")
    // console.log(addTxt);
    let notes = localStorage.getItem("notes");
    //  console.log(notes);
    if (notes == null) {
        notesObj = [];
    } else {
        notesObj = JSON.parse(notes);
        // console.log(addTxt.value);
    }
    // console.log(notesObj);
    notesObj.push(addTxt.value);
    alert("your Notes is save")
    //  console.log(addTxt);
    localStorage.setItem("notes", JSON.stringify(notesObj));
    addTxt.value = "";
    document.getElementById("addBtn").disabled = true;
    // console.log(notesObj);
    // location.reload();


});

//from locacal storege show notes
function showNotes() {
    let notes = localStorage.getItem("notes");
    if (notes == null) {
        notesObj = [];
    } else {
        notesObj = JSON.parse(notes);
    }
    let html = "";
    notesObj.forEach(function (element, index) {
        html += ` 
                <div class="noteCard card mx-3 my-3 pl-5 justify-content-center" style="width: 18rem;">
                    <div class="card-body">
                    <h6 class="card-title">Note:- ${index + 1}</h6>
                    <p class="card-text"> ${element}</p>
                    <button id="${index}" onclick="deleteNote(this.id)" class="btn btn-primary">Delete Note</button>
                </div>
                </div>
                `;

    });
    let notesElem = document.getElementById("notes");
    if (notesObj.length != 0) {
        notesElem.innerHTML = html;
    } else {
        notesElem.innerHTML = `Nothing to show `
    }
}

// Delete the notes from show
function deleteNote(index) {
    // console.log("deleteing.....", index);//ok
    // alert("Your Notes is Deleted....")
    let notes = localStorage.getItem("notes");

    if (confirm("Are You Sure You Wan't to Delete!!!") == true) {
        if (notes == null) {
            notesObj = [];
        } else {
            notesObj = JSON.parse(notes);
        }
        notesObj.splice(index, 1);
        localStorage.setItem("notes", JSON.stringify(notesObj));
        showNotes();
    }
}

// search 
search = document.getElementById("searchTxt");

search.addEventListener("input", function () {
    let inputVal = search.value.toLowerCase();//OK 
    // console.log(inputVal);
    // console.log("input event  run.....",inputVal);

    let noteCard = document.getElementsByClassName("noteCard");

    Array.from(noteCard).forEach(function (element) {
        let cardTxt = element.getElementsByTagName("p")[0].innerText;//ok
        // console.log(cardTxt, inputVal);
        if (cardTxt.toLowerCase().includes(inputVal)) {
            element.style.display = "block";
        } else {
            element.style.display = "none";
        }
    })
});
